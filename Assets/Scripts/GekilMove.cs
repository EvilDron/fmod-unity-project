﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GekilMove : MonoBehaviour
{
    [SerializeField] private float RotateSpeed = 5f;
    [SerializeField] private float Radius = 0.1f;

    [SerializeField] private Vector3 _centre;
    [SerializeField] private float _angle;


    private void Start()
    {
        _centre = transform.position;
    }

    void Update()
    {
        _angle -= RotateSpeed * Time.deltaTime;

        var offset = new Vector3(Mathf.Sin(_angle), 0, Mathf.Cos(_angle)) * Radius;

        transform.rotation = Quaternion.Euler(0, _angle  * Mathf.Rad2Deg - 90, 0);

        transform.position = _centre + offset;
    }
}