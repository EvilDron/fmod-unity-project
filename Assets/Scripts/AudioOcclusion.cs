﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class AudioOcclusion : MonoBehaviour {
     [Header("FMOD Event")]
     [FMODUnity.EventRef]
     public string SelectAudio;
     FMOD.Studio.EventInstance Audio;
     float VolumeParameter;
     float LPFParameter;

     Transform SlLocation;
  
     [Header("Occlusion Options")]
     [Range(0f, 1f)]
     public float VolumeValue = 0.5f;
     [Range(10f, 22000f)]
     public float LPFCutoff = 10000f;
     public LayerMask OcclusionLayer = 1;

     void Awake ()
     {
         SlLocation = GameObject.FindObjectOfType<StudioListener>().transform;
         Audio = FMODUnity.RuntimeManager.CreateInstance (SelectAudio);
         Audio.getParameterByName("Volume", out VolumeParameter);
         Audio.getParameterByName ("LPF", out LPFParameter);
     }

     void Start () {
         FMOD.Studio.PLAYBACK_STATE PbState;
         Audio.getPlaybackState (out PbState);
         if (PbState != FMOD.Studio.PLAYBACK_STATE.PLAYING) {
             Audio.start ();
         }
     }

     void Update () {

         FMODUnity.RuntimeManager.AttachInstanceToGameObject(Audio, GetComponent<Transform>(), GetComponent<Rigidbody>());

         RaycastHit hit;
         Physics.Linecast(transform.position, SlLocation.position, out hit, OcclusionLayer);

         if (hit.collider.name == "Player") 
         {
             //Debug.Log ("not occluded");
             NotOccluded ();
             Debug.DrawLine (transform.position, SlLocation.position, Color.blue);
         } 
         else 
         {
             //Debug.Log ("occluded");
             Occluded ();
             Debug.DrawLine (transform.position, hit.point, Color.red);
         }
     }

     void Occluded ()
     {
         VolumeParameter = VolumeValue;
         LPFParameter = LPFCutoff;
     }

     void NotOccluded ()
     {
         VolumeParameter = 1;
         LPFParameter = 22000f;
     }
}